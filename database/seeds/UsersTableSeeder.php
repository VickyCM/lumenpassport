<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
       'name' => 'Yudelmis',
            'email' => 'yudelmis@servimontajes.com',
            'password' => password_hash('Yude_Victoria', PASSWORD_BCRYPT)
        ]);

        factory(App\User::class)->create([
            'name' => 'Victoria',
            'email' => 'isabella@servimontajes.com',
            'password' => password_hash('Isa_Victoria', PASSWORD_BCRYPT)
            ]);
    }
}
